#!/bin/bash
set -e

named-checkconf /etc/bind/named.conf
named-checkzone rmt /etc/bind/db.rmt
named-checkzone 1.0.10.in-addr.arpa /etc/bind/db.10.0.1
