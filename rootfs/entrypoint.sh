#!/bin/bash
set -e

# Changing the permissions for cache dir
if [ -d /var/cache/bind ]; then
    chown root:bind /var/cache/bind
    chmod 0775 /var/cache/bind
fi

# Start the first process
echo "Downloading root.hints file ..."
wget https://www.internic.net/domain/named.root -qO- | tee /etc/bind/root.hints

echo "Starting named..."
exec $(command -v named) -u "bind" -g 
