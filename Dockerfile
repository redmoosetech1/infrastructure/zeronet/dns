FROM debian:stable-slim
MAINTAINER "RMT"

# Install Base Packages
RUN DEBIAN_FRONTEND=non-interactive apt-get update -qq -y && \
    DEBIAN_FRONTEND=non-interactive apt-get install -qq -y --no-install-recommends \
    bind9 \
    wget \
    bind9utils \
    dnsutils \
    ca-certificates \
    dns-root-data \
    iputils-ping

COPY rootfs/ /
VOLUME /var/cache/bind
EXPOSE 953/tcp \
       53/tcp \
       53/udp

RUN chmod +x /entrypoint.sh && \
    chmod +x /healthcheck.sh && \
    chmod +x /test.sh 

HEALTHCHECK --interval=30s --timeout=30s --start-period=10s \
CMD /healthcheck.sh

ENTRYPOINT ["/entrypoint.sh"]
