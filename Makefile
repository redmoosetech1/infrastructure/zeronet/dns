REGISTRY_IMAGE=redmoosetech/dns:$(shell date +"%Y%m%d")
REGISTRY_IMAGE_LATEST=redmoosetech/dns:latest


.PHONY: build start stop shell clean restart rebuild logs test deploy update destroy
default: build

build:
	sudo docker buildx use builder
	sudo docker buildx build --platform=linux/amd64,linux/arm64 -t ${REGISTRY_IMAGE} -t ${REGISTRY_IMAGE_LATEST} --push .

start:
	docker-compose up -d

stop:
	docker-compose down

shell:
	docker exec -it ${DNS_IMAGE_NAME} /bin/bash

clean:
	docker system prune -af

restart:
	docker restart ${DNS_IMAGE_NAME}

rebuild: clean build start

logs:
	docker service logs -f ${DNS_IMAGE_NAME}

test:
	docker pull redmoosetech/dns:latest
	docker run --rm -it --entrypoint=/test.sh $(REGISTRY_IMAGE)

deploy:
	kubectl apply -f manifests/dns-cache.yml
	kubectl apply -f manifests/dns.yml
	kubectl apply -f manifests/svc.yml

update:
	kubectl set image deployment/dns dns=$(REGISTRY_IMAGE) -n dns

destroy:
	kubectl delete -f manifests/

status:
	kubectl get pods -n dns
